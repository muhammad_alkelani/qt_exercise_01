#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_horizontalSlider_sliderMoved(int position )
{

	ui->checkBox->setChecked(position > 50);
}

void MainWindow::on_horizontalSlider_2_valueChanged(int Value)
{
	ui->horizontalSlider->setValue(Value);
}
